from rest_framework import serializers
from .models import ChatMessage, Thread
from django.contrib.auth import get_user_model

User = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'


class ChatSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = ChatMessage
        fields = '__all__'


class ThreadSerializer(serializers.ModelSerializer):
    chat_message = serializers.SerializerMethodField()

    class Meta:
        model = Thread
        fields = '__all__'

    def get_chat_message(self, obj):
        qs = ChatMessage.objects.filter(thread=obj)
        serializer = ChatSerializer(qs, many=True)
        return serializer.data
