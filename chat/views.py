from django.shortcuts import render
from rest_framework.views import APIView
from .models import ChatMessage, Thread
from .serializers import ThreadSerializer
from rest_framework.response import Response


def index(request):
    return render(request, 'index.html', {})


def room(request, room_name):
    return render(request, 'chatroom.html', {
        'room_name': room_name
    })


class ChatRoomApiView(APIView):

    def get(self, request):
        from_user = int(request.query_params.get('from_user'))
        to_user = int(request.query_params.get('to_user'))
        thread_qs = Thread.objects.filter(first_person=from_user, second_person=to_user)
        print(thread_qs)

        if thread_qs.exists():
            qs = thread_qs.first()
        # else:
        #     qs = Thread.objects.create(first_person=from_user, second_person=to_user)
        serializer = ThreadSerializer(qs)
        return Response(serializer.data)

