import json
from channels.generic.websocket import AsyncWebsocketConsumer
from channels.db import database_sync_to_async
from django.contrib.auth import get_user_model
from chat.models import Thread, ChatMessage

User = get_user_model()


class ChatRoomConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        user_id = self.scope['url_route']['kwargs']['user_id']

        self.user = await self.get_user_object(user_id)
        # print(user)

        chat_room = f'user_chatroom_{self.user.id}'
        self.chat_room = chat_room

        self.room_group_name = 'chat_%s' % self.room_name
        # user = self.scope['user']
        # print(user)

        await self.channel_layer.group_add(
            self.chat_room,
            self.channel_name
        )

        await self.accept()

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard(
            self.chat_room,
            self.channel_name
        )

    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        print(text_data_json)
        msg = text_data_json.get('message')
        sent_by_id = text_data_json.get('sent_by')
        send_to_id = text_data_json.get('send_to')
        thread_id = text_data_json.get('thread_id')
        # message = text_data_json['message']
        # username = text_data_json['username']

        sent_by_user = await self.get_user_object(sent_by_id)
        send_to_user = await self.get_user_object(send_to_id)
        thread_obj = await self.get_thread(thread_id)

        if not sent_by_user:
            print('Error:: sent by user is incorrect')
        if not send_to_user:
            print('Error:: send to user is incorrect')
        if not thread_obj:
            print('Error:: Thread id is incorrect')

        await self.create_chat_message(thread_obj, sent_by_user, msg)

        other_user_chat_room = f'user_chatroom_{send_to_id}'

        response = {
            'message': msg,
            'sent_by': self.user.id,
            'thread_id': thread_id
        }

        await self.channel_layer.group_send(
            self.chat_room,
            {
                'type': 'chatroom_message',
                'message': msg,
                'username': self.user.id,
            }
        )

        await self.channel_layer.group_send(
            other_user_chat_room,
            {
                'type': 'chatroom_message',
                'message': msg,
                'username': self.user.id,
                # 'text': json.dumps(response)
            }
        )

    async def chatroom_message(self, event):
        print(event)
        message = event['message']
        username = event['username']

        await self.send(text_data=json.dumps({
            'message': message,
            'username': username,
        }))

    @database_sync_to_async
    def get_user_object(self, user_id):
        qs = User.objects.filter(id=user_id)
        if qs.exists():
            obj = qs.first()
        else:
            obj = None
        return obj

    @database_sync_to_async
    def get_thread(self, thread_id):
        qs = Thread.objects.filter(id=thread_id)
        if qs.exists():
            obj = qs.first()
        else:
            obj = None
        return obj

    @database_sync_to_async
    def create_chat_message(self, thread, user, msg):
        ChatMessage.objects.create(thread=thread, user=user, message=msg)
