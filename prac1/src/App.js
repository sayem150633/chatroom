import React, { Component } from "react";
import { w3cwebsocket as W3CWebSocket } from "websocket";

import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import Paper from "@material-ui/core/Paper";
import Avatar from "@material-ui/core/Avatar";

import "./App.css";

import { withStyles } from "@material-ui/core/styles";

const useStyles = (theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  root: {
    boxShadow: "none",
  },
});

class App extends Component {
  state = {
    isLoggedIn: false,
    messages: [],
    value: "",
    name: "",
    room: "vacad",
    isOpen: false,
  };
  constructor(props) {
    super(props);
    this.messagesEndRef = React.createRef();
  }

  handleOpen = () => {
    this.setState({
      ...this.state,
      isOpen: true,
    });
  };

  handleClose = () => {
    this.setState({
      ...this.state,
      isOpen: false,
    });
  };

  // messagesEndRef = React.createRef();

  componentDidUpdate() {
    this.scrollToBottom();
  }
  scrollToBottom = () => {
    return this.messagesEndRef.current?.scrollIntoView({ behavior: "smooth" });
  };

  // client = new W3CWebSocket("ws://127.0.0.1:8000/chat/");
  client = new W3CWebSocket(
    "ws://127.0.0.1:8000/ws/chat/" + this.state.room + "/" + 1 + "/"
  );

  onButtonClicked = (e) => {
    this.client.send(
      JSON.stringify({
        type: "message",
        // message: this.state.value,
        // username: this.state.name,
        message: this.state.value,
        sent_by: 1,
        send_to: 2,
        thread_id: 1,
      })
    );
    this.state.value = "";
    e.preventDefault();
    // let chat_logs = document.getElementById("chat-logs");
    let chat_logs = document.querySelectorAll(".chat-logs");
    console.log(chat_logs);
    let last = chat_logs[chat_logs.length - 1];
    last.scrollIntoView();
    // chat_logs.scroll(0, 1000);
    // console.log(chat_logs);
  };

  componentDidMount() {
    this.scrollToBottom();
    fetch(
      "http://127.0.0.1:8000/chat/aaa/?" +
        new URLSearchParams({
          from_user: 1,
          to_user: 2,
        })
    )
      .then((res) => res.json())
      .then((data) => {
        let chatData = data.chat_message;
        console.log(chatData);
        let b = [];
        for (let i = 0; i < chatData.length; i++) {
          b.push({
            msg: chatData[i].message,
            name: chatData[i].user.email,
            id: chatData[i].user.id,
          });
        }
        console.log(b);
        this.setState((state) => ({
          messages: [...state.messages, ...b],
        }));
      })
      .catch((err) => console.log(err));

    this.client.onopen = () => {
      console.log("WebSocket Client Connected");
    };
    this.client.onmessage = (message) => {
      const dataFromServer = JSON.parse(message.data);
      // const dataFromServer2 = JSON.parse(message);
      // console.log(dataFromServer2);
      console.log("got reply! ", dataFromServer.type);
      console.log("got reply! ", dataFromServer);
      if (dataFromServer) {
        this.setState((state) => ({
          isOpen: true,
          messages: [
            ...state.messages,
            {
              msg: dataFromServer.message,
              name: dataFromServer.username,
              id: dataFromServer.username,
            },
          ],
        }));
      }
    };

    //  window.scrollTo({ top: 0, behavior: "smooth" });

    let chat_logs = document.getElementById("chat-logs");
    chat_logs.scroll(0, chat_logs[0]);
    console.log(chat_logs);
    // chat_logs.animate({ scrollTop: "100px" })
  }

  render() {
    const { classes } = this.props;
    console.log(this.state);
    console.log(this.messagesEndRef);
    return (
      <Container component="main" maxWidth="xs">
        {/* {
          <div style={{ marginTop: 50 }}>
            Room Name: {this.state.room}
            <Paper
              style={{
                height: 500,
                maxHeight: 500,
                overflow: "auto",
                boxShadow: "none",
              }}
            >
              {this.state.messages.map((message) => {

                if (message.id === 1) {
                return (
                  <>
                    <Card className={classes.root}>
                      <CardHeader
                        avatar={<Avatar className={classes.avatar}>R</Avatar>}
                        title={message.name}
                        subheader={message.msg}
                      />
                    </Card>
                  </>
                );
                } else {
                  return (
                    <div style={{ marginLeft: "133px" }}>
                      <Card className={classes.root}>
                        <CardHeader
                          avatar={<Avatar className={classes.avatar}>R</Avatar>}
                          title={message.name}
                          subheader={message.msg}
                        />
                      </Card>
                    </div>
                  );
              }
                 
              })}
            </Paper>
            <form
              className={classes.form}
              noValidate
              onSubmit={this.onButtonClicked}
            >
              <TextField
                id="outlined-helperText"
                label="Make a comment"
                defaultValue="Default Value"
                variant="outlined"
                value={this.state.value}
                fullWidth
                onChange={(e) => {
                  this.setState({ value: e.target.value });
                  this.value = this.state.value;
                }}
              />
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
              >
                Start Chatting
              </Button>
            </form>
          </div>
        } */}

        <div id="center-text">
          <h2>ChatBox UI</h2>
          <p>Message send and scroll to bottom enabled </p>
        </div>

        <div
          onClick={this.handleOpen}
          id={this.state.isOpen ? "scale" : "chat-circle"}
          class="btn btn-raised"
        >
          <div id="chat-overlay"></div>
          <i class="material-icons">speaker_phone</i>
        </div>

        <div class={this.state.isOpen ? "scale" : "chat-box"}>
          <div class="chat-box-header">
            ChatBot
            <span class="chat-box-toggle" onClick={this.handleClose}>
              <i class="material-icons">close</i>
            </span>
          </div>
          <div class="chat-box-body">
            <div class="chat-box-overlay"></div>
            <div class="chat-logs" id="chat-logs">
              {this.state.messages.map((message) => {
                return (
                  <div
                    className={
                      message.id === 1 ? "chat-msg self" : "chat-msg other"
                    }
                  >
                    <span className="msg-avatar">
                      <img src="https://media.istockphoto.com/vectors/user-icon-flat-isolated-on-white-background-user-symbol-vector-vector-id1300845620?k=20&m=1300845620&s=612x612&w=0&h=f4XTZDAv7NPuZbG0habSpU0sNgECM0X7nbKzTUta3n8=" />
                    </span>
                    <div class="cm-msg-text">{message.msg}</div>
                  </div>
                );
              })}
              <div ref={this.messagesEndRef} />
            </div>
          </div>
          <div class="chat-input">
            <form onSubmit={this.onButtonClicked}>
              <input
                type="text"
                id="chat-input"
                placeholder="Send a message..."
                value={this.state.value}
                onChange={(e) => {
                  this.setState({ value: e.target.value });
                  this.value = this.state.value;
                }}
              />
              <button type="submit" class="chat-submit">
                <i class="material-icons">send</i>
              </button>
            </form>
          </div>
        </div>
      </Container>
    );
  }
}
export default withStyles(useStyles)(App);
